To add the schema to the openldap server:
```
ldapadd -D "<CONFIGURATION ADMIN DM>" -x -W -f /etc/ldap/schema/asat.ldif
```

To modify the already existing schema, remove the identical values from `asat.ldif` (including `cn`), rename the `dn` to the actual one used by LDAP (e.g. `cn={4}asat`...), and run:
```
ldapvi -D "<CONFIGURATION ADMIN DM>" --in /etc/ldap/schema/asat.ldif
```
or:
```
sudo ldapvi --sasl-mech EXTERNAL --host ldapi:/// --in asat.ldif
```

(tested with OpenLDAP)
